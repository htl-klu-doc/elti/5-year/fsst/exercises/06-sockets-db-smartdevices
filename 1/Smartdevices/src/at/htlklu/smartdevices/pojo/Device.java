package at.htlklu.smartdevices.pojo;

public class Device {
	private int id;
	private String name;
	private boolean isOn = false;

	public Device(int id, String name, boolean isOn) {
		this.setId(id);
		this.setName(name);
		this.setOn(isOn);
	}

	@Override
	public String toString() {
		return String.format("%s:%s", this.getName(), this.isOnString());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOn() {
		return isOn;
	}

	public String isOnString() {
		return this.isOn ? "ON" : "OFF";
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}
}
