package at.htlklu.smartdevices.db;

import at.htlklu.smartdevices.pojo.Device;

public class Helper {
	public static String getDeviceCmd(String data) {
		try {
			int id = Integer.parseInt(data);
			Device device = DeviceDB.getDeviceById(id);
			if (device == null)
				return "Unknown device";

			return device.toString();
		} catch (NumberFormatException e) {
			return String.format("Invalid id [%s]", data);
		}
	}

	public static String registerCmd(String name) {
		Device device = new Device(-1, name, false);
		int status = DeviceDB.insertDevice(device);

		if (status != 1)
			return "Could not register device";

		return String.format("REGISTRATION OF »%s« OK:ID=%d", device.getName(), device.getId());
	}
}
