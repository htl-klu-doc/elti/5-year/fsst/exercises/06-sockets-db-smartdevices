package at.htlklu.smartdevices.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import at.htlklu.smartdevices.pojo.Device;

public class DeviceDB {

	public static Device getDeviceById(int id) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("SELECT * FROM device WHERE id_device = ?")) {

			pstmt.setInt(1, id);
			ResultSet rset = pstmt.executeQuery();

			if (rset.next()) {
				return mapRow(rset);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static Device mapRow(ResultSet rset) throws SQLException {
		return new Device(rset.getInt(1), rset.getString(2), rset.getBoolean(3));
	}

	public static int insertDevice(Device device) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("INSERT INTO device (name, is_on) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {

			pstmt.setString(1, device.getName());
			pstmt.setBoolean(2, device.isOn());
			int status = pstmt.executeUpdate();
			ResultSet rset = pstmt.getGeneratedKeys();
			if (rset.next())
				device.setId(rset.getInt(1));
			return status;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

}
