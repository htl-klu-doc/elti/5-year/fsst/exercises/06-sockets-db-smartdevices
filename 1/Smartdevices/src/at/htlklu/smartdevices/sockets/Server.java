package at.htlklu.smartdevices.sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import at.htlklu.smartdevices.db.Helper;

public class Server extends Thread {
	static final int PORT = 5710;

	private BufferedReader in;
	private PrintWriter out;
	private boolean keepRunning = true;

	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(PORT);

			while (true) {
				Server s = new Server(serverSocket.accept());
				s.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Server(Socket s) {
		try {
			this.in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			this.out = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (keepRunning) {
			out.println(communicate());
		}
	}

	private String communicate() {
		try {
			String input = in.readLine();

			if (input == null)
				return "Empty input!";

			String[] tokens = input.split(" ");

			if (tokens.length == 1) {
				if (tokens[0].equals("EXIT")) {
					keepRunning = false;
					return "Bye";
				} else
					return "UNKNOWN COMMAND!";
			}

			switch (tokens[0]) {
			case "GETSTATUS":
				return Helper.getDeviceCmd(tokens[1]);
			case "REGISTER":
				return Helper.registerCmd(input.substring(tokens[0].length() + 1));

			default:
				return "UNKNOWN COMMAND!";
			}

		} catch (IOException e) {
			e.printStackTrace();
			keepRunning = false;
			return "Error :(";
		}
	}
}
